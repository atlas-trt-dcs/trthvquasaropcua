# LICENSE:
# Copyright (c) 2015, CERN
# All rights reserved.
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
# THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
# GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Author: Paris Moschovakos <paris.moschovakos@cern.ch>

option(LOGIT_BACKEND_UATRACE "Whether to build with UATRACE (logging via UASDK logger)" ON)

add_definitions( -Wno-unused-local-typedefs -DBACKEND_UATOOLKIT )

#------
#OPCUA
#------
if(DEFINED ENV{OPCUA_TOOLKIT_PATH})
    message ("Taking OPC UA Toolkit path from the environment: $ENV{OPCUA_TOOLKIT_PATH}")
    SET( OPCUA_TOOLKIT_PATH $ENV{OPCUA_TOOLKIT_PATH} )
else(DEFINED ENV{OPCUA_TOOLKIT_PATH})
    SET( OPCUA_TOOLKIT_PATH "/opt/OpcUaToolkit-1.6.5" )
    message ("Taken OPC UA Toolkit path: ${OPCUA_TOOLKIT_PATH}") 
endif(DEFINED ENV{OPCUA_TOOLKIT_PATH})

SET( OPCUA_TOOLKIT_LIBS_DEBUG "-lxml2 -lssl -lcrypto -lpthread -lrt -luamoduled -lcoremoduled -luapkicppd -luabasecppd -luastackd -lxmlparsercppd" )
SET( OPCUA_TOOLKIT_LIBS_RELEASE "-lxml2 -lssl -lcrypto -lpthread -lrt -luamodule -lcoremodule -luapkicpp -luabasecpp -luastack -lxmlparsercpp" )

include_directories (
${OPCUA_TOOLKIT_PATH}/include/uastack
${OPCUA_TOOLKIT_PATH}/include/uabasecpp
${OPCUA_TOOLKIT_PATH}/include/uaservercpp
${OPCUA_TOOLKIT_PATH}/include/xmlparsercpp
${OPCUA_TOOLKIT_PATH}/include/uapkicpp
)
link_directories( ${OPCUA_TOOLKIT_PATH}/lib )

# ------------------------------------------
# quasar Debugging facilities
# ------------------------------------------
set(CMAKE_VERBOSE_MAKEFILE OFF)
option(ENABLE_QUASAR_SANITIZERS "Enable quasar sanitizers" OFF)

option(ENABLE_SANITIZER_ADDRESS "Enable address sanitizer" OFF)
option(ENABLE_SANITIZER_LEAK "Enable leak sanitizer" OFF)
option(ENABLE_SANITIZER_UNDEFINED_BEHAVIOR "Enable undefined behavior sanitizer" OFF)
option(ENABLE_SANITIZER_THREAD "Enable thread sanitizer" OFF)
option(ENABLE_SANITIZER_MEMORY "Enable memory sanitizer" OFF)
option(ENABLE_COVERAGE "Enable coverage" ON)

#-----
#XML Libs
#-----
#As of 03-Sep-2015 I see no FindXerces or whatever in our Cmake 2.8 installation, so no find_package can be user...
# TODO perhaps also take it from environment if requested
SET( XML_LIBS "-lxerces-c" )

#-----
#General settings
#-----

# TODO: split between Win / MSVC, perhaps MSVC has different notation for these
add_definitions(-Wall -Wno-deprecated -std=gnu++0x )

set( GIT_SERVER_REPLACEMENT "https://gitlab.cern.ch/" CACHE STRING "obtain git-hosted packages from gitlab.cern.ch mirrors")

add_custom_target( quasar_opcua_backend_is_ready )

#-----
# CanModule (via ISEGHAL)
#-----
set( CANMODULE_BUILD_VENDORS "ON" )
set( CANMODULE_BUILD_SYSTEC "ON" )
set( CANMODULE_BUILD_PEAK "OFF" )
set( CANMODULE_BUILD_ANAGATE "OFF" )
