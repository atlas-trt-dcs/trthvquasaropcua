#include <Configurator.h>
#include <Configuration.hxx>
#include <boost/lexical_cast.hpp>
#include <fstream>
#include <algorithm>
#include <ConfigurationDecorationUtils.h>

class UserSpecificConfigurationHandler
{
public:
    bool operator()(Configuration::Configuration& theConfiguration)
    {
        Configuration::Configuration::HVROOT_sequence& rootseq (theConfiguration.HVROOT());
        for(Configuration::Configuration::HVROOT_iterator i (rootseq.begin()); i != rootseq.end(); ++i)
        {
            Configuration::Configuration::HVROOT_type& hvroot (*i);
            LOG(Log::INF) << "HVROOT NAME: " << hvroot.name();

            Configuration::HVROOT::PORT_sequence& portseq (hvroot.PORT());
            for(Configuration::HVROOT::PORT_iterator j (portseq.begin()); j != portseq.end(); ++j)
            {
                Configuration::HVROOT::PORT_type& port (*j);
                LOG(Log::INF) << "PORT NAME: " << port.name();

                Configuration::PORT::CRATE_sequence& crateseq (port.CRATE());
                //crateseq.push_back(Configuration::CRATE("Crate"));
                Configuration::DecorationUtils::push_back(
                	port,
					port.CRATE(),
					Configuration::CRATE("Crate"),
					Configuration::PORT::CRATE_id
					);
                LOG(Log::INF)<<" SIZE CRATE "<<crateseq.size();
                for(Configuration::PORT::CRATE_iterator k (crateseq.begin()); k != crateseq.end(); ++k)
                    {
                  	    // LOG(Log::INF)<< " Crate itr "<< k;
                         Configuration::PORT::CRATE_type& crate (*k);
                         LOG(Log::INF) << "CRATE NAME: " << crate.name();
                    }

                Configuration::PORT::BRANCH_sequence& branchseq (port.BRANCH());
                for(Configuration::PORT::BRANCH_iterator k (branchseq.begin()); k != branchseq.end(); ++k)
                {
                    Configuration::PORT::BRANCH_type& branch (*k);
                    LOG(Log::INF) << "BRANCH NAME: " << branch.name();

                    Configuration::BRANCH::BOARD_sequence& boardseq (branch.BOARD());
                    Configuration::BRANCH::CELL_sequence& cellseq (branch.CELL());

                    for(Configuration::BRANCH::BOARD_iterator l (boardseq.begin()); l != boardseq.end(); ++l)
                    {
                        Configuration::BRANCH::BOARD_type& board (*l);
                        LOG(Log::INF) << "BOARD NAME: " << board.name();
                        // CALIB file for BOARD -> open
                        const char* label = board.CalibrationLabel().c_str();
                        int boardno = label[strlen(label)-1] - '0'; //extract board number from calib label
                        //LOG(Log::INF) << boardno;
                        bool noCalibFlag = false;
                        std::fstream calibFile;
                        std::string line;
                        std::string calibFilePath;

                        if(board.CalibrationLabel() == "NoCalib")
                        {
                            LOG(Log::INF) << "NoCalib specified, initializing board with default calibration values";
                            noCalibFlag = true;
                        }
                        else
                        {
                            std::string calibFileName = "HV_calib_";
                            calibFileName.append(board.CalibrationLabel());
                            calibFileName.append("_");
                            calibFileName.append(board.CalibrationDate());
                            calibFileName.append(".txt");
                            //LOG(Log::INF) << "Calibration file name: " << calibFileName;

                            calibFilePath = hvroot.CalibrationPath();
                            calibFilePath.append("/");
                            calibFilePath.append(calibFileName);
                            LOG(Log::INF) << "Calibration file path: " << calibFilePath;

                            calibFile.open(calibFilePath.c_str(), std::fstream::in);

                            if(!calibFile.is_open())
                            {
                                LOG(Log::ERR) << "Calibration file (" << calibFilePath << ") not found, shutting down";
                                return false;
                            }
                            else
                            {
                            	LOG(Log::INF) << "Calibration file (" << calibFilePath << ") opened";
                            }

                            do
                            {
                                getline(calibFile, line);
                            }
                            while(line.at(0) == '#');
                        }


                        // CELLS for BOARD -> create
                        // and
                        // CALIB constants -> load

                        for(int it = 1; it <= 21; it++)
                        {
                            std::vector<float> params;
                            std::string fileCellNo;
                            if(!noCalibFlag)
                            {
                                int idPos = std::min(line.find_first_of('0'),line.find_first_of('1')); //find cellID position
                                fileCellNo = line.substr(idPos, 3);
                                //LOG(Log::INF) << fileCellNo;
                                std::istringstream stm(line.substr(idPos + 5)); //substr only with floats (skip cell id)
                                //LOG(Log::INF) << stm.str();
                                float f;
                                while(stm >> f)
                                {
                                    params.push_back(f);
                                    if(stm.peek() == ',') stm.ignore();
                                }
                            }
                            else
                            {
                                params.push_back(1.0);
                                params.push_back(0.0);
                                params.push_back(1.0);
                                params.push_back(0.0);
                            }


                            Configuration::CELL::name_type name;
                            name = "HVCell";
                            int cellno = it + (boardno - 1) * 21;
                            std::string stringCellNo = boost::lexical_cast<std::string>(cellno);
                            name.append(stringCellNo);

                            if(stringCellNo.size() == 1)
                            {
                                char first = stringCellNo.at(0);
                                stringCellNo = "00";
                                stringCellNo.push_back(first);
                            }
                            if(stringCellNo.size() == 2)
                            {
                                char first = stringCellNo.at(0);
                                char second = stringCellNo.at(1);
                                stringCellNo = "0";
                                stringCellNo.push_back(first);
                                stringCellNo.push_back(second);
                            }

                            if(noCalibFlag || (!noCalibFlag && stringCellNo.compare(fileCellNo) == 0))
                            {
                                Configuration::CELL cell = Configuration::CELL(name,
                                        stringCellNo, params[0], params[1], params[2], params[3]);
                                LOG(Log::INF) << "Added cell: " << name << ", ID: " << stringCellNo.c_str() << ", DAC slope/offset: "
                                        << params[0] << "/" << params[1] << "   ADC slope/offset: " << params[2] << "/" << params[3];
                                //cellseq.push_back(cell);
                                Configuration::DecorationUtils::push_back(
                                branch,
								branch.CELL(),
								Configuration::CELL(cell),
								Configuration::BRANCH::CELL_id);
                            }
                            else
                            {
                                LOG(Log::ERR) << "Cell IDs mismatch, expected " << stringCellNo << ", got "
                                        << fileCellNo << " from calibration file (" << calibFilePath << "), shutting down.";
                                return false;
                            }

                            if(!noCalibFlag)
                            {
                                getline(calibFile, line);
                            }
                        }

                        calibFile.close();
                    }
                }
            }
        }
        return true;
    }
};
