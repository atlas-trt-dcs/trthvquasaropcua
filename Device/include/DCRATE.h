/*
 * DCRATE.h
 *
 *  Created on: Nov 6, 2023
 *      Author: root
 */

#ifndef DEVICE_INCLUDE_DCRATE_H_
#define DEVICE_INCLUDE_DCRATE_H_
#include <vector>
#include <boost/thread/mutex.hpp>
#include <statuscode.h>
#include <session.h>
#include <DRoot.h>
#include <Configuration.hxx>

#include <Base_DCRATE.h>

#include "ComTransaction.h"
#include "ComMessage.h"
#include "ComPortObject.h"
#include "HVSysDefinitions.h"
#include <pthread.h>
#include <unistd.h>
#include <LogIt.h>

using namespace std;

namespace Device
{
 class
   DCRATE
    :public Base_DCRATE
	 {
	 public:
	   explicit DCRATE(const Configuration::CRATE &config,Parent_DCRATE * parent);

	   ~DCRATE();

	   UaStatus writeBREFSwitch (const OpcUa_Boolean &v);

	   UaStatus writeMonThreadEnable (const OpcUa_Boolean &v);

	 private:
	     /* Delete copy constructor and assignment operator */
	     DCRATE( const DCRATE & );
	     DCRATE& operator=(const DCRATE &other);

	     // ----------------------------------------------------------------------- *
	     // -     CUSTOM CODE STARTS BELOW THIS COMMENT.                            *
	     // -     Don't change this comment, otherwise merge tool may be troubled.  *
	     // ----------------------------------------------------------------------- *

	 public:
	         ItemsDefs*                      getItemsDefs()   	{ return idefs; };
	         ComPortObject*					getComPortObject() 	{ return m_pcp; };
	 		void 							enableMonThread() 	{m_enable = OpcUa_True;}
	 		void 							disableMonThread() 	{m_enable = OpcUa_False;}
	 		const char*						getPortID()			{ return m_portID.c_str(); }
	 		OpcUa_Boolean					getEnable()			{ return m_enable; }
	 		OpcUa_Int64						getMonEmptyCycles()	{ return m_monEmptyCycles; }
	 		bool							getRunThread()		{ return m_runThread; }
	 		void							stopMonThread(void);		//{ m_runThread = false; pthread_join(m_hThread,0); return;}
	 		void							startMonThread()	{ m_runThread = true;}


	 private:
	         ItemsDefs		*idefs;
	         ComPortObject	*m_pcp;
	 		OpcUa_Boolean 	m_stop;
	 		OpcUa_Boolean	m_enable;
	 		OpcUa_Int64 	m_monEmptyCycles;
	 		std::string		m_portID;
	 	    unsigned char 	cb;
	 	    unsigned char 	cc;
	 		volatile bool   m_runThread;
	 		pthread_t		m_hThread;
	 		int				m_idThread;
	 		static void*	monThread(void*);

	 };
}

#endif /* DEVICE_INCLUDE_DCRATE_H_ */
