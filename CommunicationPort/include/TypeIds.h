/******************************************************************************
** Copyright (C) 20011 CERN Rights Reserved.
** Web: http://www.cern.ch
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
** Project: CanOpen UA OPC server
**
** Description: Numeric Id definitions for building automation type nodes.
******************************************************************************/

/************************************************************
 Root Type and its instance declaration
*************************************************************/
#define Ba_RootType						1000
// Instance Declaration
#define Ba_RootType_ConfigVersion		1001
#define Ba_RootType_StackTrackEnable	1002
#define Ba_RootType_StackTrackLevel		1003
#define Ba_RootType_AppTrackEnable		1004
#define Ba_RootType_AppTrackLevel		1005
#define Ba_RootType_MaxTrackEntries		1006
#define Ba_RootType_TrackMaxBackup		1007
#define Ba_RootType_AppTraceFile		1008

/************************************************************
 Port Type and its instance declaration
*************************************************************/
// Port Node Type
#define Ba_PortType						1100
// Instance declaration
#define Ba_PortType_BaudRate			1101
#define Ba_PortType_DataBits			1102
#define Ba_PortType_StopBits			1103
#define Ba_PortType_Parity				1104
#define Ba_PortType_CommunicationBus	1105
/************************************************************/

/************************************************************
 Crate Type and its instance declaration
*************************************************************/
// Crate Node Type
#define Ba_CrateType					1150
// Instance declaration
#define Ba_CrateType_ControllerType		1151
#define Ba_CrateType_ExtHVEnableStatus	1152
#define Ba_CrateType_ExtVoltValue		1153
#define Ba_CrateType_ExtVoltRef			1154
#define Ba_CrateType_OverheatingStatus	1155
#define Ba_CrateType_BVOverheatTrip		1156
#define Ba_CrateType_ExtRefStatus		1157
#define Ba_CrateType_BVINLLTrip			1158
#define Ba_CrateType_BVINULTrip			1159
#define Ba_CrateType_BREFLLTrip			1160
#define Ba_CrateType_BREFULTrip			1161
#define Ba_CrateType_IntTemp			1162
#define Ba_CrateType_ExtTempA			1163
#define Ba_CrateType_ExtTempB			1164
#define Ba_CrateType_BREFSwitch			1165
/************************************************************/

/************************************************************
 Branch Type and its instance declaration
*************************************************************/
// Branch Node Type
#define Ba_BranchType						1200
// Instance declaration
#define Ba_BranchType_BaseVoltValue			1201
#define Ba_BranchType_LowVoltValue			1202
#define Ba_BranchType_BaseVoltSwitch		1203
#define Ba_BranchType_BaseVoltStatus		1204
#define Ba_BranchType_LowVoltSwitch			1205
#define Ba_BranchType_LowVoltStatus			1206
/************************************************************/

/************************************************************
 Board Type and its instance declaration
*************************************************************/
// Board Type
#define Ba_BoardType						1300
// Instance declaration
#define Ba_BoardType_CalibLabel				1301
#define Ba_BoardType_CalibDate				1302

/************************************************************/

/************************************************************
Cell Type and its instance declaration
*************************************************************/
// Cell Node Type
#define Ba_CellType							1400
// Instance declaration
#define Ba_CellType_HVSetpoint				1401
#define Ba_CellType_ThresholdSetpoint		1402
#define Ba_CellType_VoltageValue			1403
#define Ba_CellType_CurrentValue			1404
#define Ba_CellType_RampUpSetpoint			1405
#define Ba_CellType_RampDownSetpoint		1406
#define Ba_CellType_StandbyVoltSetpoint		1407
#define Ba_CellType_ProtectDelaySetpoint	1408
#define Ba_CellType_CellSwitch				1409
#define Ba_CellType_Error					1410
#define Ba_CellType_GeneratorStatus			1411
#define Ba_CellType_AccumError				1412
#define Ba_CellType_CurrentOverload			1413
#define Ba_CellType_StandbyStatus			1414
#define Ba_CellType_RampDownStatus			1415
#define Ba_CellType_RampUpStatus			1416
/************************************************************/


/************************************************************
 Views
*************************************************************/
// AirConditioner View
//#define Ba_AirConditionerView                            5000
// Furnace View
//#define Ba_FurnaceView                                   5001
// ControllerEventType

#define Ba_Crate							6000
